﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Demo;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class DemoTests
    {
        private TestServer _server;
        private HttpClient _client;

        [SetUp]
        public void Setup()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }
        
        [Test]
        public async Task Test()
        {
            var customer = @"{
            ""name"": ""string"",
            ""email"": ""string""
            }";
            
            var result = await _client.PostAsync("/api/values", 
                new StringContent(customer, Encoding.UTF8, "application/json"));

            var customers = JArray.Parse(await _client.GetStringAsync("/api/values"));
            
            result.StatusCode.Should().Be(HttpStatusCode.OK);
            customers.Should().NotBeEmpty();

            customers[0]["name"].Value<string>().Should().Be("string");
            customers[0]["email"].Value<string>().Should().Be("string");
        }
        
    }
}