﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Demo.Models
{
    public abstract class EntityBase
    {
        public int Id { get; set; }
    }
    
    public class Customer : EntityBase
    {
        public string Name { get; set; }
        
        public string Email { get; set; }

        public virtual IReadOnlyCollection<Order> Orders { get; set; }

        public void AddOrder(decimal amount)
        {
            var orders = (ICollection<Order>)Orders;
        }
    }

    [Owned]
    public class Order : EntityBase
    {
        public int CustomerId { get; set; }
        
        public decimal Amount { get; set; }
        
        public DateTime CreatedOn { get; set; }
    }
}